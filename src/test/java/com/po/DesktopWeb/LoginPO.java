/**
 * 
 */
package com.po.DesktopWeb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.test.common.BasePO;

/**
 * @author akumar
 *
 */
public class LoginPO extends BasePO {
	public WebDriver driver;

	// Constructor
	public LoginPO(WebDriver driver) {
		super(driver);
	}

	@FindBy(name = "uid")
	private WebElement username;

	@FindBy(name = "password")
	private WebElement password;

	@FindBy(name = "btnLogin")
	private WebElement loginBtn;

	public void loginDetails() throws InterruptedException {
		username.sendKeys("mngr195463");
		password.sendKeys("rUzyqem");
		Thread.sleep(4000);
		loginBtn.click();
	}

}
