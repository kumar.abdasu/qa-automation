/**
 * 
 */
package com.test.common;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * @author akumar
 *
 */
public class BaseTest {
	public static WebDriver driver = null;

	@BeforeTest
	public void setup() throws IOException {
		System.setProperty("webdriver.chrome.driver", "E:\\Care\\qa-automation\\src\\main\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/V4/index.php");
		driver.manage().window().maximize();
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
